package com.impactsolutions.leskan.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.facebook.drawee.interfaces.DraweeController
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.request.ImageRequest
import com.impactsolutions.leskan.R
import com.impactsolutions.leskan.models.Consultation
import android.net.Uri
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.impactsolutions.leskan.HomeActivity
import com.impactsolutions.leskan.global_functions.showLongToast
import com.impactsolutions.leskan.global_functions.verifyNetwork

class ConsultationsAdapter(
    val adapterContext: Context,
    val consultations: Array<Consultation>,
    val home: HomeActivity
) : BaseAdapter() {

    override fun getCount(): Int {
        return consultations.size
    }

    override fun getItem(position: Int): Any {
        return consultations[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {

        var consultation: Consultation = consultations[position]

        var convertV = LayoutInflater.from(adapterContext).inflate(R.layout.element_consultation, null)

        val tvDate: TextView = convertV.findViewById(R.id.elementConsultationTvDate)
        val imageGlide : ImageView = convertV.findViewById(R.id.elementConsultationIv);

        if (verifyNetwork(home as AppCompatActivity)) {
            convertV.setOnClickListener { home.openConsultationDetail(consultation.image) }
        } else {
            convertV.setOnClickListener { showLongToast(adapterContext, "Por favor conéctate a internet para ver los detalles de la consulta" ) }
        }

        tvDate.text = consultation.date

        Glide.with(adapterContext).asBitmap()
            .load(consultation.image)
            .into(imageGlide)

        return convertV
    }
}