package com.impactsolutions.leskan.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.impactsolutions.leskan.HomeActivity
import com.impactsolutions.leskan.R
import com.impactsolutions.leskan.global_functions.showLongToast
import com.impactsolutions.leskan.global_functions.verifyNetwork
import com.impactsolutions.leskan.models.Tracing

class TracingsAdapter(
    val adapterContext: Context,
    val tracings: Array<Tracing>,
    val home: HomeActivity
) : BaseAdapter() {

    override fun getCount(): Int {
        return tracings.size
    }

    override fun getItem(position: Int): Any {
        return tracings[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {

        var tracing: Tracing = tracings[position]

        var convertV = LayoutInflater.from(adapterContext).inflate(R.layout.element_tracing, null)

        if (verifyNetwork(home as AppCompatActivity)) {
            convertV.setOnClickListener { home.openTraceDetail(tracing.name) }
        } else {
            convertV.setOnClickListener { showLongToast(adapterContext, "Por favor conéctate a internet para ver los detalles del seguimiento" ) }
        }

        val tvName: TextView = convertV.findViewById(R.id.elementTracingTvName)
        val tvDate: TextView = convertV.findViewById(R.id.elementTracingTvDate)
        val ivState: ImageView = convertV.findViewById(R.id.elementsTracingIv)

        tvName.text = tracing.name
        tvDate.text = tracing.date

        if(tracing.state != "LOADING") {
            ivState.setImageResource(R.drawable.ic_info_blue_28dp)
        }

        return convertV
    }
}