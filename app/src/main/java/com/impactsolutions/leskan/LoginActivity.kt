package com.impactsolutions.leskan

import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.*
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.impactsolutions.leskan.global_functions.showLongToast
import com.impactsolutions.leskan.global_functions.showNetworkErrorToast
import com.impactsolutions.leskan.global_functions.updateCurrentUser
import com.impactsolutions.leskan.global_functions.verifyAvailableNetwork

class LoginActivity : AppCompatActivity() {

    private lateinit var txtEmail: EditText
    private lateinit var txtPassword: EditText
    private lateinit var loginButton: Button
    private lateinit var progressBar: ProgressBar
    private lateinit var tvResetPassword: TextView
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        verifyAvailableNetwork(this, false)
        setContentView(R.layout.activity_login)

        // Make the reference to the view variables
        txtEmail = findViewById(R.id.loginTiedEmail)
        txtPassword = findViewById(R.id.loginTiedPassword)
        txtPassword.typeface = Typeface.DEFAULT
        txtPassword.transformationMethod = PasswordTransformationMethod()
        progressBar = findViewById(R.id.loginProgressBar)
        loginButton = findViewById(R.id.loginButton)
        tvResetPassword = findViewById(R.id.tv_reset_password)
        auth = FirebaseAuth.getInstance()
    }

    // Function called by the button
    fun logIn(view: View) {
        if (verifyAvailableNetwork(this, true)) {
            logInUser()
        }
    }

    // Goes to ResetPassword activity
    fun goToResetPassword(view: View) {
        if (verifyAvailableNetwork(this, true)) {
            startActivity(Intent(this, ResetPasswordActivity::class.java))
        }
    }

    private fun logInUser() {
        val email: String = txtEmail.text.toString()
        val password: String = txtPassword.text.toString()

        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            enableLoading(true)

            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->

                if (task.isSuccessful) {
                    goToHome()
                } else {
                    Log.w("x", task.exception)
                    Log.w("x", task.exception!!.message)
                    try {
                        throw task.exception!!
                    } catch (exception: FirebaseAuthException) {
                        val code = exception.errorCode
                        Log.w(code, exception.message)

                        // Verifies the error code
                        when (code) {
                            "ERROR_INVALID_EMAIL" -> {
                                txtEmail.error = "La dirección de correo que ingresaste no es válida"
                            }
                            "ERROR_WRONG_PASSWORD" -> {
                                txtPassword.error = "La contraseña no es correcta"
                            }
                            "ERROR_USER_NOT_FOUND" -> {
                                txtEmail.error = "No existe un usuario con el correo ingresado"
                            }
                            else -> {
                                showLongToast(this, "Hubo un error en el inicio de sesión. Intenta nuevamente")
                            }
                        }
                    } catch (exception: FirebaseNetworkException) {
                        showNetworkErrorToast(this)
                    } catch (exception: Exception) {
                        showLongToast(this, "Hubo un error en el inicio de sesión. Intenta nuevamente")
                    }
                    enableLoading(false)
                }
            }

        } else {
            Toast.makeText(this, "Por favor completa todos los campos", Toast.LENGTH_LONG).show()
        }
    }

    // Goes to Home activity
    private fun goToHome() {
        updateCurrentUser()
        finishAffinity()
        startActivity(Intent(this, MainActivity::class.java))
    }

    // Finish current activity
    fun finishActivityLogin(view: View) {
        finish()
    }

    private fun enableLoading(enable: Boolean) {
            txtEmail.isEnabled = !enable
            txtPassword.isEnabled = !enable

            if(enable){
                progressBar.visibility = View.VISIBLE
                loginButton.visibility = View.GONE
            } else {
                progressBar.visibility = View.GONE
                loginButton.visibility = View.VISIBLE
            }
    }
}
