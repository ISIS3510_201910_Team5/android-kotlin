package com.impactsolutions.leskan

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.impactsolutions.leskan.global_functions.isLoggedIn
import com.impactsolutions.leskan.global_functions.showLongToast
import com.impactsolutions.leskan.global_functions.showNetworkErrorToast
import com.impactsolutions.leskan.global_functions.verifyAvailableNetwork
import com.impactsolutions.leskan.models.User
import kotlinx.android.synthetic.main.activity_register.*
import java.text.SimpleDateFormat
import java.util.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var txtFirstName: EditText
    private lateinit var txtLastName: EditText
    private lateinit var txtEmail: EditText
    private lateinit var txtPassword: EditText
    private lateinit var txtRepeatPassword: EditText
    private lateinit var txtSex: RadioGroup
    private lateinit var txtBirthDate: EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var dataBase: FirebaseFirestore
    private lateinit var auth: FirebaseAuth
    private lateinit var registerButton: Button
    private lateinit var backArrow: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        verifyAvailableNetwork(this, false)

        if (isLoggedIn()) {
            finish()
            startActivity(Intent(this, FirstTimeActivity::class.java))
        }

        setContentView(R.layout.activity_register)

        // Make the reference to the view variables
        txtFirstName = findViewById(R.id.registerTiedFirstName)
        txtLastName = findViewById(R.id.registerTiedLastName)
        txtEmail = findViewById(R.id.registerTiedEmail)
        txtPassword = findViewById(R.id.registerTiedPassword)
        txtPassword.typeface = Typeface.DEFAULT
        txtPassword.transformationMethod = PasswordTransformationMethod()
        txtRepeatPassword = findViewById(R.id.registerTiedRepeatPassword)
        txtRepeatPassword.typeface = Typeface.DEFAULT
        txtRepeatPassword.transformationMethod = PasswordTransformationMethod()
        txtBirthDate = findViewById(R.id.registerTiedBirthDate)
        txtSex = findViewById(R.id.registerRgSex)
        progressBar = findViewById(R.id.registerProgressBar)
        registerButton = findViewById(R.id.registerButton)
        backArrow = findViewById(R.id.registerIvBackArrow)
        dataBase = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        generateCalendar()
    }

    // Function called by the button
    fun register(view: View) {
        if(verifyAvailableNetwork(this, true)) {
            createNewUser()
        }
    }

    private fun generateCalendar() {
        var cal = Calendar.getInstance()

        val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val myFormat = "dd/MM/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            txtBirthDate.setText(sdf.format(cal.time),  TextView.BufferType.EDITABLE)
        }

        txtBirthDate.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (currentFocus!= null) {
                    val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                }

                var datePickerDialog = DatePickerDialog(this@RegisterActivity, dateSetListener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
                val maxDate = Calendar.getInstance()
                maxDate.set(Calendar.YEAR, maxDate.get(Calendar.YEAR) - 12)
                datePickerDialog.datePicker.maxDate = maxDate.timeInMillis
                datePickerDialog.setTitle("")
                datePickerDialog.show()
            }
        })
    }

    // Creates a new user in the database
    private fun createNewUser() {
        // Gets the value of the variables
        val firstName: String = txtFirstName.text.toString()
        val lastName: String = txtLastName.text.toString()
        val email: String = txtEmail.text.toString()
        val password: String = txtPassword.text.toString()
        val repeatPassword: String = txtRepeatPassword.text.toString()
        val birthDate: String = txtBirthDate.text.toString()
        val selectedSex = txtSex.checkedRadioButtonId

        if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(repeatPassword) && !TextUtils.isEmpty(birthDate) && selectedSex != -1)
        {
           if(password == repeatPassword) {
               val sex: String = (findViewById<RadioButton>(selectedSex)).text.toString()
               disableInputs(false)
               registerButton.visibility = View.GONE
               registerIvBackArrow.visibility = View.GONE
               progressBar.visibility = View.VISIBLE


               auth.createUserWithEmailAndPassword(email, password)
                   .addOnCompleteListener(this) { task ->
                       if (task.isSuccessful) {
                           val user: FirebaseUser? = auth.currentUser;
                           if (user != null) {
                               saveUser(user, email, firstName, lastName, birthDate, sex, password)
                           } else {
                               showLongToast(this, "Hubo un error en el registro")
                               progressBar.visibility = View.GONE
                               registerButton.visibility = View.VISIBLE
                               registerIvBackArrow.visibility = View.VISIBLE
                           }

                       } else {
                           try {
                               throw task.exception!!
                           } catch (exception: FirebaseAuthException) {
                               val code = exception.errorCode
                               Log.w(code, exception.message)

                               // Verifies the error code
                               when (code) {
                                   "ERROR_INVALID_EMAIL" -> {
                                       txtEmail.error = "La dirección de correo que ingresaste no es válida"
                                   }
                                   "ERROR_WEAK_PASSWORD" -> {
                                       txtPassword.error = "La contraseña debe ser de mínimo 6 caracteres"
                                   }
                                   "ERROR_EMAIL_ALREADY_IN_USE" -> {
                                       txtEmail.error = "Ya existe un usuario con el correo que ingresaste"
                                   }
                                   else -> {
                                       showLongToast(this, "Hubo un error en el registro. Intenta nuevamente")
                                   }
                               }
                           } catch (exception: FirebaseNetworkException) {
                               showNetworkErrorToast(this)
                           } catch (exception: Exception) {
                               showLongToast(this, "Hubo un error en el registro. Intenta nuevamente")
                           }

                           progressBar.visibility = View.GONE
                           registerButton.visibility = View.VISIBLE
                           registerIvBackArrow.visibility = View.VISIBLE
                           disableInputs(true)
                       }


                   }
           } else {
               txtRepeatPassword.error = "La contraseña no coincide"
           }
        } else {
            showLongToast(this, "Por favor completa todos los campos")
        }
    }

    // Save the user on the database
    private fun saveUser(user: FirebaseUser, email: String, firstName: String, lastName: String, birthDate: String, sex: String, password: String) {
        var newSex = 1
        if(sex.equals("Femenino")) {
            newSex = 0
        }
        val newUser = User(email, firstName, lastName, newSex, birthDate, 0.0, "", false, false, user.uid)
        dataBase.collection("USER").document(user.uid).set(newUser).addOnCompleteListener {
                task ->
            if (task.isSuccessful) {
                loginUser(email, password)
            } else {
                showLongToast(this, "Hubo un error registrando tus datos")
                user.delete()
                progressBar.visibility = View.GONE
                registerButton.visibility = View.VISIBLE
                registerIvBackArrow.visibility = View.VISIBLE
            }
        }
    }

    // Log in user and goes to first activity page
    private fun loginUser(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) {
                task ->

            if(task.isSuccessful) {
                showLongToast(this, "Registro exitoso")
                finishAffinity()
                startActivity(Intent(this, FirstTimeActivity::class.java))
            } else {
                showLongToast(this, "Hubo un problema validando el registro. Inicia sesión manualmente")
                finish()
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }

    }

    // Finish current activity
    fun finishActivity(view: View) {
        finish()
    }

    private fun disableInputs(enable: Boolean) {
        txtFirstName.isEnabled = enable
        txtLastName.isEnabled = enable
        txtEmail.isEnabled = enable
        txtPassword.isEnabled = enable
        txtRepeatPassword.isEnabled = enable
        txtBirthDate.isEnabled = enable
        txtSex.getChildAt(0).isEnabled = enable
        txtSex.getChildAt(1).isEnabled = enable
    }

}
