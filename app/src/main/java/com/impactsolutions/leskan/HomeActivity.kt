package com.impactsolutions.leskan

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView

import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputEditText
import com.impactsolutions.leskan.fragments.*
import java.text.SimpleDateFormat
import java.util.*


class HomeActivity : AppCompatActivity() {

    lateinit var fragment: Fragment

    var currentFragment: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigation_view)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        fragment = TracingsFragment.newInstance()
        currentFragment = "T"
        openFragment(fragment)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        when (item.itemId) {
            R.id.navigation_profile -> {
                if(currentFragment == "P") {
                    return@OnNavigationItemSelectedListener true
                } else {
                    supportFragmentManager.beginTransaction().remove(fragment).commit()
                    supportFragmentManager.popBackStack()
                    currentFragment = "P"
                    fragment = ProfileFragment.newInstance()
                    openFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            R.id.navigation_newConsultation -> {
                if(currentFragment == "N") {
                    return@OnNavigationItemSelectedListener true
                } else {
                    supportFragmentManager.beginTransaction().remove(fragment).commit()
                    supportFragmentManager.popBackStack()
                    currentFragment = "N"
                    fragment = NewConsultationFragment.newInstance()
                    openFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            R.id.navigation_tracings-> {
                if(currentFragment == "T") {
                    return@OnNavigationItemSelectedListener true
                } else {
                    supportFragmentManager.beginTransaction().remove(fragment).commit()
                    supportFragmentManager.popBackStack()
                    currentFragment = "T"
                    fragment = TracingsFragment.newInstance()
                    openFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            else -> {
                fragment = TracingsFragment.newInstance()
                openFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
    }

    override fun onBackPressed() {
        openFragment(fragment)
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun openEditProfile() {
        val editFragment = EditprofileFragment.newInstance()
        openFragment(editFragment)
    }

    fun openTraceDetail(name: String) {
        val traceDetail = TraceDetailFragment.newInstance(name)
        openFragment(traceDetail)
    }

    fun openProfile() {
        val profileFragment = ProfileFragment.newInstance()
        openFragment(profileFragment)
    }

    fun openConsultationDetail(imagen: String) {
        val consultationDetail = ConsultationDetailFragment.newInstance(imagen)
        openFragment(consultationDetail)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun generateCalendar(tiedBirthDate : TextInputEditText) {
        var cal = Calendar.getInstance()

        val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val myFormat = "dd/MM/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            tiedBirthDate.setText(sdf.format(cal.time),  TextView.BufferType.EDITABLE)
        }

        tiedBirthDate.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (currentFocus!= null) {
                    val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                }

                var datePickerDialog = DatePickerDialog(this@HomeActivity, dateSetListener, cal.get(Calendar.YEAR), cal.get(
                    Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
                val maxDate = Calendar.getInstance()
                maxDate.set(Calendar.YEAR, maxDate.get(Calendar.YEAR) - 12)
                datePickerDialog.datePicker.maxDate = maxDate.timeInMillis
                datePickerDialog.setTitle("")
                datePickerDialog.show()
            }
        })
    }
}
