package com.impactsolutions.leskan.fragments

import android.os.Bundle

import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.impactsolutions.leskan.HomeActivity
import com.impactsolutions.leskan.R
import com.impactsolutions.leskan.adapters.TracingsAdapter
import com.impactsolutions.leskan.models.Tracing
import com.impactsolutions.leskan.global_functions.updateCurrentUser
import com.impactsolutions.leskan.global_functions.verifyAvailableNetwork

class TracingsFragment : Fragment() {

    lateinit var listView: ListView
    lateinit var textViewEmpty: TextView
    lateinit var imageViewEmpty: ImageView
    lateinit var tracingsAdapter: TracingsAdapter
    var tracings: ArrayList<Tracing> = ArrayList<Tracing>()
    lateinit var user: FirebaseUser
    lateinit var dataBase: FirebaseFirestore

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        verifyAvailableNetwork(this.requireActivity() as AppCompatActivity, false, local = true)
        var view = inflater.inflate(R.layout.fragment_tracings, container, false)
        ButterKnife.bind(this, view)
        updateCurrentUser()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listView = view.findViewById(R.id.fragmentTracingsLv)
        textViewEmpty = view.findViewById(R.id.fragmentsTracingTv)
        imageViewEmpty = view.findViewById(R.id.fragmentsTracingIv)
        val dummyTracing = Tracing("Cargando...", "NONE", "", "", "", "Cargando...")
        listView.adapter = TracingsAdapter(this.requireContext(), arrayOf(dummyTracing), activity as HomeActivity)
        var currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser != null) {
            user = currentUser
        } else {
            this.requireActivity().finishAffinity()
        }
        dataBase = FirebaseFirestore.getInstance()
        getTracings()
    }

    private fun getTracings() {
        val docRef = dataBase.collection("USER").document(user.uid).collection("TRACING")
            docRef.get()
            .addOnSuccessListener { result ->
                if (result.size() == 0) {
                    listView.visibility = View.GONE
                    textViewEmpty.visibility = View.VISIBLE
                    imageViewEmpty.visibility = View.VISIBLE

                } else {
                    tracings = ArrayList()
                    for (document in result) {
                        var tracing = Tracing(document.data["name"] as String, document.data["state"] as String, document.data["colorChange"] as String, document.data["sizeChange"] as String, document.data["textureChange"] as String,   document.data["date"] as String)
                        tracings.add(tracing)
                    }
                    listView.adapter = TracingsAdapter(this.requireContext(), tracings.toTypedArray(), activity as HomeActivity)
                }
            }
            .addOnFailureListener { exception ->
                listView.visibility = View.GONE
                textViewEmpty.visibility = View.VISIBLE
                imageViewEmpty.visibility = View.VISIBLE
                Log.d("b", "Error getting documents: ", exception)
            }

    }

    companion object {
        fun newInstance(): TracingsFragment =
            TracingsFragment()
    }
}
