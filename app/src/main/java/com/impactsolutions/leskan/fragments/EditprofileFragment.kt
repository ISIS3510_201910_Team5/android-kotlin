package com.impactsolutions.leskan.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.impactsolutions.leskan.HomeActivity
import com.impactsolutions.leskan.R
import com.impactsolutions.leskan.global_functions.currentUser
import com.impactsolutions.leskan.global_functions.showLongToast
import com.impactsolutions.leskan.global_functions.verifyAvailableNetwork

class EditprofileFragment : Fragment() {

    private lateinit var tiedEmail: TextInputEditText
    private lateinit var tiedFirstName: TextInputEditText
    private lateinit var tiedLastName: TextInputEditText
    private lateinit var tiedBirthDate: TextInputEditText
    private lateinit var rbMasculino: RadioButton
    private lateinit var rbFemenino: RadioButton
    private lateinit var rbAns1Yes: RadioButton
    private lateinit var rbAns1No: RadioButton
    private lateinit var rbAns2Yes: RadioButton
    private lateinit var rbAns2No: RadioButton
    private lateinit var rgSex: RadioGroup
    private lateinit var rgAns1: RadioGroup
    private lateinit var rgAns2: RadioGroup
    private lateinit var progressBar: ProgressBar
    private lateinit var db: FirebaseFirestore
    private lateinit var auth: FirebaseAuth
    private lateinit var editButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        verifyAvailableNetwork(this.requireActivity() as AppCompatActivity, false)
        return inflater.inflate(R.layout.fragment_editprofile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()
        tiedEmail = view.findViewById(R.id.editprofileTiedEmail)
        tiedEmail.setText(currentUser.email)
        tiedEmail.isEnabled = false
        tiedFirstName = view.findViewById(R.id.editprofileTiedFirstName)
        tiedFirstName.setText(currentUser.firstName)
        tiedLastName = view.findViewById(R.id.editprofileTiedLastName)
        tiedLastName.setText(currentUser.lastName)
        tiedBirthDate = view.findViewById(R.id.editprofileTiedBirthDate)
        tiedBirthDate.setText(currentUser.birthDate)

        rbFemenino = view.findViewById(R.id.editprofileRbFemenino)
        rbMasculino = view.findViewById(R.id.editprofileRbMasculino)
        if(currentUser.sex == 0) rbFemenino.isChecked = true
        else rbMasculino.isChecked = true

        rbAns1Yes = view.findViewById(R.id.editprofileRbAnswer1Yes)
        rbAns1No = view.findViewById(R.id.editprofileRbAnswer1No)
        if(currentUser.commonIssue == false) rbAns1No.isChecked = true
        else rbAns1Yes.isChecked = true

        rbAns2Yes = view.findViewById(R.id.editprofileRbAnswer2Yes)
        rbAns2No = view.findViewById(R.id.editprofileRbAnswer2No)
        if(currentUser.familySkinCancer == false) rbAns2No.isChecked = true
        else rbAns2Yes.isChecked= true

        editButton = view.findViewById(R.id.editprofileButton)
        editButton.setOnClickListener { v -> updateUser() }

        rgSex = view.findViewById(R.id.editprofileRgSex)
        rgAns1 = view.findViewById(R.id.editprofileRgAnswer1)
        rgAns2 = view.findViewById(R.id.editprofileRgAnswer2)

        progressBar = view.findViewById(R.id.editprofileProgressBar)

        generateCalendar()
    }

    fun generateCalendar(){
        var activity: HomeActivity = activity as HomeActivity
        activity.generateCalendar(tiedBirthDate)
        this.activity!!.currentFocus
    }

    fun updateUser(){
        if(verifyAvailableNetwork(this.requireActivity() as AppCompatActivity, true)) {
            var userAuth = auth.currentUser
            val sfDocRef = db.collection("USER").document(userAuth!!.uid)

            var newFirstName : String = tiedFirstName.text.toString()
            var newLastName : String = tiedLastName.text.toString()
            var newBirthDate : String = tiedBirthDate.text.toString()
            var newSex = 0
            if(rbMasculino.isChecked) newSex= 1
            var newAns1 : Boolean = rbAns1Yes.isChecked
            var newAns2 : Boolean = rbAns2Yes.isChecked


            db.runTransaction { transaction ->
                enableLoading(true)
                transaction.update(sfDocRef, "firstName", newFirstName, "lastName", newLastName, "birthDate", newBirthDate , "commonIssue" , newAns1, "familySkinCancer", newAns2, "sex", newSex)
                // Success
                null
            }.addOnSuccessListener {
                showLongToast(requireContext().applicationContext,"Edición exitosa" )
                currentUser.firstName = newFirstName
                currentUser.lastName = newLastName
                currentUser.birthDate= newBirthDate
                currentUser.commonIssue = newAns1
                currentUser.familySkinCancer = newAns2
                currentUser.sex = newSex
                goToProfile()
            }
                .addOnFailureListener { e -> Log.w("ERROR", "Transaction failure.", e)
                    showLongToast(requireContext().applicationContext,"Edición Fallida" )
                    enableLoading(false)}
        }
    }

    private fun goToProfile() {
        var activity: HomeActivity = activity as HomeActivity
        activity.openProfile()
    }

    companion object {
        fun newInstance(): EditprofileFragment = EditprofileFragment()
    }

    private fun enableLoading(enable: Boolean) {
        activity!!.runOnUiThread {
            tiedFirstName.isEnabled = !enable
            tiedLastName.isEnabled = !enable
            tiedBirthDate.isEnabled = !enable
            rgSex.isEnabled = !enable
            rgAns1.isEnabled = !enable
            rgAns2.isEnabled = !enable
            if(enable){
                progressBar.visibility = View.VISIBLE
                editButton.visibility = View.GONE
            }else{
                progressBar.visibility = View.GONE
                editButton.visibility = View.VISIBLE
            }
        }
    }


}
