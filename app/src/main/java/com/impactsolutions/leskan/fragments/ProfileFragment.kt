package com.impactsolutions.leskan.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.impactsolutions.leskan.HomeActivity
import com.impactsolutions.leskan.MainActivity
import com.impactsolutions.leskan.R
import com.impactsolutions.leskan.global_functions.currentUser
import com.impactsolutions.leskan.global_functions.verifyAvailableNetwork


class ProfileFragment : Fragment() {

    private lateinit var tvName: TextView
    private lateinit var tvEmail: TextView
    private lateinit var tvAnswer1: TextView
    private lateinit var tvAnswer2: TextView
    private lateinit var tvBirthDate: TextView
    private lateinit var tvGender: TextView
    private lateinit var ivEdit: ImageView
    private lateinit var auth: FirebaseAuth
    private lateinit var ivLogOut: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        verifyAvailableNetwork(this.requireActivity() as AppCompatActivity, false, local = true)
        tvName = view.findViewById(R.id.profileTvName)
        tvName.setText(currentUser.firstName + " " + currentUser.lastName)
        tvEmail = view.findViewById(R.id.profileTvEmail)
        tvEmail.setText(currentUser.email)
        tvBirthDate = view.findViewById(R.id.profileTvBirthdate)
        tvBirthDate.setText(currentUser.birthDate)
        tvGender = view.findViewById(R.id.profileTvGender)
        if (currentUser.sex == 0) tvGender.setText("Femenino")
        else tvGender.setText("Masculino")
        tvAnswer1 = view.findViewById(R.id.profileTvAnswer1)
        if (currentUser.commonIssue == false) tvAnswer1.setText("No")
        else tvAnswer1.setText("Si")
        tvAnswer2 = view.findViewById(R.id.profileTvAnswer2)
        if (currentUser.familySkinCancer == false) tvAnswer2.setText("No")
        else tvAnswer2.setText("Si")

        ivEdit = view.findViewById(R.id.profileIvEdit)
        ivEdit.setOnClickListener { v ->
            goToEditProfile(view)
        }

        ivLogOut = view.findViewById(R.id.profileIVLogOut)
        ivLogOut.setOnClickListener { _ ->
            logOut(view)
        }

        auth = FirebaseAuth.getInstance()
    }

    companion object {
        fun newInstance(): ProfileFragment = ProfileFragment()
    }

    private fun logOut(view: View) {
        auth.signOut()
        val intent = Intent(activity, MainActivity::class.java)
        this.requireActivity().finishAffinity()
        startActivity(intent)
    }

    private fun goToEditProfile(view: View) {
        if( verifyAvailableNetwork(this.requireActivity() as AppCompatActivity, true)) {
            var activity: HomeActivity = activity as HomeActivity
            activity.openEditProfile()
        }
    }

}
