package com.impactsolutions.leskan.fragments

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import butterknife.ButterKnife
import com.google.android.material.chip.Chip
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.impactsolutions.leskan.HomeActivity
import com.impactsolutions.leskan.R
import com.impactsolutions.leskan.adapters.ConsultationsAdapter
import com.impactsolutions.leskan.global_functions.userId
import com.impactsolutions.leskan.global_functions.verifyAvailableNetwork
import com.impactsolutions.leskan.models.Consultation
import com.impactsolutions.leskan.models.Tracing
import java.util.*
import kotlin.collections.ArrayList


class TraceDetailFragment : Fragment(){

    private lateinit var tvName: TextView
    private lateinit var tvDate: TextView
    private lateinit var chipState: Chip
    private lateinit var tvGrowthDetail: TextView
    private lateinit var tvGrowthRatio: TextView
    private lateinit var tvColorDetail: TextView
    private lateinit var tvColorRatio: TextView
    private lateinit var tvFormDetail: TextView
    private lateinit var tvFormhRatio: TextView
    private lateinit var lvConsultations: ListView
    private lateinit var tvConsultationsEmpty: TextView
    private lateinit var ivArrowEmpty: ImageView
    private lateinit var db: FirebaseFirestore
    private lateinit var trace: DocumentReference
    var consultations: ArrayList<Consultation> = ArrayList<Consultation>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        verifyAvailableNetwork(this.requireActivity() as AppCompatActivity, false, local = true)
        var view = inflater.inflate(R.layout.fragment_trace_detail, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvName = view.findViewById(R.id.tracedetailTvName)
        tvDate = view.findViewById(R.id.tracedetailTvDate)
        chipState = view.findViewById(R.id.tracedetailTvState)
        tvGrowthDetail= view.findViewById(R.id.tracedetailTvGrowthDetail)
        tvGrowthRatio = view.findViewById(R.id.tracedetailTvGrowthRatio)
        tvColorDetail= view.findViewById(R.id.tracedetailTvColorDetail)
        tvColorRatio = view.findViewById(R.id.tracedetailTvColorRatio)
        tvFormDetail= view.findViewById(R.id.tracedetailTvFormDetail)
        tvFormhRatio = view.findViewById(R.id.tracedetailTvFormRatio)
        lvConsultations = view.findViewById(R.id.tracedetailLvConsultations)
        tvConsultationsEmpty = view.findViewById(R.id.tracedetailTvNoConsultations)
        ivArrowEmpty = view.findViewById(R.id.tracedetailIvArrow)

        db = FirebaseFirestore.getInstance()
        trace = db.collection("USER").document(userId).collection("TRACING").document(arguments?.get("name") as String)

        trace.get().addOnSuccessListener{docSnapShot ->
            if(docSnapShot.exists()){
                var info = docSnapShot.toObject(Tracing::class.java)
                if(info != null) {
                    tvName.text = info.name
                    tvDate.text = info.date

                    var state = info.state
                    var color = R.color.colorDanger
                    var textColor = Color.WHITE

                    if(state == "LOADING") {
                        state = "CARGANDO"
                        color = R.color.colorLoading
                    } else if(state == "DANGER") {
                        state = "PELIGRO"
                    } else if(state == "NO_DANGER") {
                        state = "NO ES PELIGROSO"
                        color = R.color.colorNoDanger
                    } else if(state == "POTENTIALLY_DANGER") {
                        state = "POTENCIALMENTE PELIGROSO"
                        color = R.color.colorPotentiallyDanger
                    } else {
                        state = "ESTADO NO RECONOCIDO"
                        textColor = Color.BLACK
                        color = R.color.colorNoState
                    }

                    chipState.text = state
                    chipState.chipBackgroundColor = ColorStateList.valueOf(ContextCompat.getColor(this.requireContext(), color))
                    chipState.setTextColor(textColor)

                    var sizeChange = info.sizeChange
                    var colorChange = info.colorChange
                    var textureChange = info.textureChange

                    if(sizeChange.isEmpty()) {
                        sizeChange = "0.0"
                    }

                    if(colorChange.isEmpty()) {
                        colorChange = "0.0"
                    }

                    if(textureChange.isEmpty()) {
                        textureChange = "0.0"
                    }


                    tvGrowthDetail.text = "Aumento del " + sizeChange + " % en el tamaño respecto a la primera consulta"
                    tvGrowthRatio.text = sizeChange + " %"
                    tvColorDetail.text = "Variación del " +  colorChange + " % en el color respecto a la primera consulta"
                    tvColorRatio.text = colorChange + " %"
                    tvFormDetail.text = "Cambio del " +  textureChange + " % en la forma respecto a la primera consulta"
                    tvFormhRatio.text = textureChange + " %"
                } else {
                    tvName.text = "Seguimiento no encontrado"
                }
            }
        }

        val dummyConsultation = Consultation("Cargando...", "NONE", "", "", "", "Cargando...")
        lvConsultations.adapter = ConsultationsAdapter(this.requireContext(), arrayOf(dummyConsultation), activity as HomeActivity)
        getConsultations()
    }

    private fun getConsultations() {
        val consultationsDoc = trace.collection("CONSULTATION")
        consultationsDoc.get()
            .addOnSuccessListener { result ->
                if (result.size() == 0) {
                    enableNoConsultations(true)
                } else {
                    consultations = ArrayList<Consultation>()
                    for (document in result) {
                        var consultation = Consultation(document.data["image"] as String, document.data["date"] as String, document.data["state"] as String, document.data["country"] as String, document.data["city"] as String,   document.data["id"] as String)
                        consultations.add(consultation)
                        Log.w("INFO", "----------->" + consultation.date)
                    }
                    lvConsultations.adapter = ConsultationsAdapter(this.requireContext(), consultations.toTypedArray(), activity as HomeActivity)
                    justifyListViewHeightBasedOnChildren(lvConsultations)
                }
            }
            .addOnFailureListener { exception ->
                enableNoConsultations(true)
                Log.d("b", "Error getting documents: ", exception)
            }
    }

    companion object {
        @JvmStatic
        fun newInstance(name: String) =
            TraceDetailFragment().apply {
                arguments = Bundle().apply {
                    putString("name", name)
                }
            }
    }

    fun justifyListViewHeightBasedOnChildren(listView: ListView) {

        val adapter = listView.adapter ?: return

        val vg = listView
        var totalHeight = 0
        for (i in 0 until adapter.count) {
            val listItem = adapter.getView(i, null, vg)
            listItem.measure(0, 0)
            totalHeight += listItem.measuredHeight
        }

        val par = listView.layoutParams
        par.height = totalHeight + listView.dividerHeight * (adapter.count - 1)
        listView.layoutParams = par
        listView.requestLayout()
    }


    private fun enableNoConsultations(enable: Boolean) {
        activity!!.runOnUiThread {
            tvConsultationsEmpty.isEnabled = enable
            ivArrowEmpty.isEnabled = enable
            lvConsultations.isEnabled = !enable
        }
    }


}
