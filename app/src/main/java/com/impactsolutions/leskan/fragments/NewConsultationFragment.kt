package com.impactsolutions.leskan.fragments

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.Region
import com.facebook.common.util.UriUtil
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.impactsolutions.leskan.HomeActivity
import com.impactsolutions.leskan.MainActivity
import com.impactsolutions.leskan.R
import com.impactsolutions.leskan.global_functions.*
import com.impactsolutions.leskan.models.Consultation
import com.impactsolutions.leskan.models.Tracing
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Exception
import kotlin.math.log

class NewConsultationFragment : Fragment() {

    @BindView(R.id.newConsultationImage)
    lateinit var newConsultationImage: SimpleDraweeView
    val cameraRequestCode = 0
    var imageFilePath: String = ""
    private lateinit var newConsultationNameTil: TextInputLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var spinner: Spinner
    lateinit var newConsultationName: EditText
    private lateinit var s3: AmazonS3Client
    private lateinit var transferUtility: TransferUtility
    private lateinit var transferObserver: TransferObserver
    private lateinit var dataBase: FirebaseFirestore
    private lateinit var user: FirebaseUser
    private lateinit var consultationImage: String
    private lateinit var confirmButton: Button
    private lateinit var functions: FirebaseFunctions
    private lateinit var timeStamp: String
    private lateinit var changeModeButton: ImageView
    private lateinit var galleryButton: Button
    private lateinit var cameraButton: Button
    var imageUploaded: Boolean = false
    var showInput: Boolean = true
    var userTracings: MutableList<String> = ArrayList()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        verifyAvailableNetwork(this.requireActivity() as AppCompatActivity, false)
        var view = inflater.inflate(R.layout.fragment_new_consultation, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var currentUser = FirebaseAuth.getInstance().currentUser
        dataBase = FirebaseFirestore.getInstance()
        if (currentUser != null) {
            user = currentUser
        } else {
            requireActivity().finishAffinity()
            startActivity(Intent(requireContext(), MainActivity::class.java))
        }
        newConsultationNameTil = view.findViewById(R.id.newConsultationTilConsultationName)
        spinner = view.findViewById(R.id.newConsultationNameSpinner)
        loadData()
        changeModeButton = view.findViewById(R.id.newConsultationIvChangeMode)
        changeModeButton.setOnClickListener { v -> changeMode() }
        newConsultationName = view.findViewById(R.id.newConsultationTiedName)
        progressBar = view.findViewById(R.id.newConsultationPb)
        progressBar.getProgressDrawable().setColorFilter(
            Color.parseColor("#3256AA"), android.graphics.PorterDuff.Mode.SRC_IN
        )
        confirmButton = view.findViewById(R.id.newConsultationMbConfirm)
        functions = FirebaseFunctions.getInstance()
        galleryButton = view.findViewById(R.id.newConsultationMbGaleria)
        cameraButton = view.findViewById(R.id.newConsultationMbCamera)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data);
        var path : String
        if(resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            val uri = data?.data
            path = uri.toString()

            Log.w("Im1", imageFilePath)
            Log.w("Ur1", path)

            val cursor: Cursor = this.context!!.getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            imageFilePath = cursor.getString(idx)
            cursor.close()

            Log.w("Im2", imageFilePath)
        }else{
            Log.w("subida", imageFilePath)
            val imgUri = Uri.Builder()
                .scheme(UriUtil.LOCAL_FILE_SCHEME)
                .path(imageFilePath)
                .build()
            path = imgUri.toString()
        }

        try {
            newConsultationImage.setImageURI(path)
        } catch (e: Exception) {
            Log.w("Image exception", e.message.toString())
            showLongToast(requireContext().applicationContext, "Hubo un error cargando la imagen tomada")
        }

    }

    private fun informTracingCreationFunction(
        urls: List<String>,
        tracing: String,
        consultation: String
    ): Task<HashMap<String, Object>> {
        // Create the arguments to the callable function.


        val data = hashMapOf(
            "user" to user.uid,
            "urls" to urls,
            "tracing" to tracing,
            "consultation" to consultation,
            "isSimulated" to true
        )

        return functions
            .getHttpsCallable("rapidResponseAnalizer")
            .call(data)
            .continueWith { task ->
                val result = task.result?.data as HashMap<String, Object>
                result

            }
    }

    private fun informTracingCreation(urls: List<String>, tracing: String, consultation: String) {
        informTracingCreationFunction(urls, tracing, consultation)
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details
                    }
                    Log.w("Err", e)

                    showLongToast(requireContext().applicationContext, "Hubo un error creando el seguimiento")
                    disableHideInputs(true, View.VISIBLE, View.GONE)
                } else {
                    var file = File(imageFilePath)
                    file.delete()
                    imageFilePath = ""
                    newConsultationImage.setImageURI("")
                    newConsultationName.text = null

                    disableHideInputs(true, View.VISIBLE, View.GONE)
                    if(showInput) {
                        showLongToast(
                            requireContext().applicationContext,
                            "El seguimiento fue creado de forma exitosa. Te avisaremos cuando tengamos el análisis."
                        )
                    } else {
                        showLongToast(
                            requireContext().applicationContext,
                            "La consulta fue agregada al seguimiento de forma exitosa. Te avisaremos cuando tengamos el análisis."
                        )
                    }
                }
            })
    }

    companion object {

        private val IMAGE_PICK_CODE = 1000
        private val PERMISSION_CODE = 1001
        fun newInstance(): NewConsultationFragment =
            NewConsultationFragment()
    }

    @OnClick(R.id.newConsultationMbConfirm)
    fun saveNewConsultation() {
        if (verifyAvailableNetwork(this.requireActivity() as AppCompatActivity, true)) {
            var consultationName: String = ""
            if(showInput) {
                consultationName = newConsultationName.text.toString()
            } else if(spinner.selectedItem != null) {
                consultationName = spinner.selectedItem.toString()
            }
            if (imageFilePath != "" && consultationName != "") {

                if (consultationName.contains(".") || consultationName.contains("/") || consultationName.contains("__.*__")) {
                    showLongToast(
                        requireContext(),
                        "El nombre no puede contener puntos(.), slash(/) o guiones bajos(_)"
                    )
                } else {
                    disableHideInputs(false, View.GONE, View.VISIBLE)
                    if (imageUploaded) {
                        saveInformation(consultationName, timeStamp)
                    } else {
                        saveImageToS3(consultationName)
                    }
                }

            } else {
                showLongToast(requireContext().applicationContext, "Por favor completa todos los campos")
            }
        }
    }

    private fun saveImageToS3(consultationName: String) {
        s3 = AmazonS3Client(getCredentials(), Region.SA_SaoPaulo.toAWSRegion())
        transferUtility = TransferUtility.builder().s3Client(s3).context(requireContext().applicationContext).build()

        val date = System.currentTimeMillis()
        val normalDate = Date(date)
        val format = SimpleDateFormat("dd/MM/yyyy - HH:mm:ss")
        timeStamp = format.format(normalDate)
        consultationImage = user.uid + "/tracings/" + consultationName + "/" + date.toString() + ".jpg"
        var file = File(imageFilePath)

        showLongToast(requireContext(), "Subiendo la imagen")

        Log.w("existe", file.exists().toString())

        if(!file.exists()){
            Log.w("image", imageFilePath)
            Log.w("external", Environment.getExternalStorageDirectory().path)
            file = File(Environment.getExternalStorageDirectory().path, imageFilePath)
            Log.w("exists", file.exists().toString())
        }

        if(file.exists()) {
            transferObserver = transferUtility.upload(
                getAWSBucketName(),  //this is the bucket name on S3
                consultationImage,
                file,
                CannedAccessControlList.PublicRead
            )

            transferObserver.setTransferListener(object : TransferListener {
                override fun onStateChanged(id: Int, state: TransferState?) {
                    if (TransferState.COMPLETED == state) {
                        Log.i("UPLOAD_STATE", "COMPLETED")
                        if (verifyAvailableNetwork(requireActivity() as AppCompatActivity, false)) {
                            saveInformation(consultationName, timeStamp)
                        } else {
                            showLongToast(requireContext().applicationContext, "Hubo un error creando el seguimiento")
                            disableHideInputs(true, View.VISIBLE, View.GONE)
                        }
                    }
                }

                override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {

                    val percentage = bytesCurrent.toInt() / bytesTotal.toInt() * 100
                    progressBar.progress = percentage
                    Log.w("percentage", "" + percentage)
                }

                override fun onError(id: Int, ex: Exception?) {
                    if (verifyNetwork(requireActivity() as AppCompatActivity)) {
                        showLongToast(requireContext().applicationContext, "Hubo un error creando el seguimiento")
                        disableHideInputs(true, View.VISIBLE, View.GONE)
                    } else {
                        showLongToast(
                            requireContext().applicationContext,
                            "No se pudo subir la imagen. Por favor revisa tu conexión a internet"
                        )
                        disableHideInputs(true, View.VISIBLE, View.GONE)
                    }
                }
            })
        } else {
            showLongToast(
                requireContext().applicationContext,
                "Por favor verifica que la imagen seleccionada sea válida"
            )
            disableHideInputs(true, View.VISIBLE, View.GONE)
        }
    }

    private fun saveInformation(consultationName: String, timestamp: String) {
        val tracingRef = dataBase.collection("USER").document(user.uid).collection("TRACING").document(consultationName)

        val tracing = Tracing(consultationName, "LOADING", "", "", "", timestamp)

        val s3Image = s3.getUrl(getAWSBucketName(), consultationImage).toExternalForm()

        showLongToast(requireContext(), "Procesando la información")

        tracingRef.set(tracing)
            .addOnSuccessListener {
                val consultationRef = tracingRef.collection("CONSULTATION").document()
                val consultationId = consultationRef.id
                val consultation =
                    Consultation(s3Image, timestamp, "LOADING", "", "", consultationId)
                consultationRef.set(consultation)
                    .addOnSuccessListener {
                        tracingRef.collection("CONSULTATION").get()
                            .addOnSuccessListener { documents ->
                                val images: MutableList<String> = ArrayList()
                                for (document in documents) {
                                    images.add(document.data["image"].toString())
                                }
                                informTracingCreation(images, consultationName, consultationId)
                            }
                            .addOnFailureListener { exception ->
                                showLongToast(requireContext().applicationContext, "No fue posible crear el seguimiento")
                            }

                    }.addOnFailureListener { e ->
                        showLongToast(requireContext().applicationContext, "No fue posible crear el seguimiento")
                    }
            }
            .addOnFailureListener { e ->
                showLongToast(requireContext().applicationContext, "No fue posible crear el seguimiento")
            }
    }


    // Opens the camera for taking a picture
    @OnClick(R.id.newConsultationMbCamera)
    fun openCamera() {
        if (verifyAvailableNetwork(this.requireActivity() as AppCompatActivity, true)) {
            if (!imageUploaded) {

                var dexter = Dexter.withActivity(this.activity)
                    .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                    )
                    .withListener(object : MultiplePermissionsListener {

                        override fun onPermissionRationaleShouldBeShown(
                            permissions: MutableList<PermissionRequest>?,
                            token: PermissionToken?
                        ) {

                            try {

                                var alertDialog = AlertDialog.Builder(requireContext().applicationContext)
                                    .setTitle("getString(R.string.storage_permission_rationale_title)")
                                    .setMessage("getString(R.string.storage_permission_rationale_message)")
                                    .setNegativeButton(
                                        android.R.string.cancel,
                                        DialogInterface.OnClickListener { dialogInterface, i ->
                                            dialogInterface.dismiss()
                                            token?.cancelPermissionRequest()
                                        })
                                    .setPositiveButton(
                                        android.R.string.ok,
                                        DialogInterface.OnClickListener { dialogInterface, i ->
                                            dialogInterface.dismiss()
                                            token?.continuePermissionRequest()
                                        })

                                Log.w("aaaa", "c")
                                alertDialog.show()
                            } catch (e: Exception) {
                                showLongToast(requireContext(), "Activa los permisos y reinicia la aplicación para continuar")
                                var intentSettings = Intent(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", requireContext().packageName, null)
                                )
                                intentSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                startActivity(intentSettings)
                            } catch (er: Error) {
                                showLongToast(requireContext(), "Activa los permisos y reinicia la aplicación para continuar")
                                var intentSettings = Intent(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", requireContext().packageName, null)
                                )
                                intentSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                startActivity(intentSettings)
                            }
                        }

                        override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                            if (report?.areAllPermissionsGranted()!!) {
                                try {
                                    val imageFile = createImageFile()
                                    val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                    if (callCameraIntent.resolveActivity(requireContext().packageManager) != null) {
                                        val authorities = requireContext().packageName + ".fileprovider"
                                        val imageUri =
                                            FileProvider.getUriForFile(
                                                requireContext().applicationContext,
                                                authorities,
                                                imageFile
                                            )
                                        callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                                        startActivityForResult(callCameraIntent, cameraRequestCode)
                                    }
                                } catch (e: IOException) {
                                    showLongToast(requireContext().applicationContext, "No se pudo crear la imagen")
                                }
                            } else {
                                showLongToast(
                                    requireContext(),
                                    "Sin los permisos de la camára no puedes hacer consultas"
                                )
                            }

                        }

                    }
                    )


                dexter.withErrorListener { error ->
                    showLongToast(requireContext(), "Activa los permisos y reinicia la aplicación para continuar")
                    var intentSettings = Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", requireContext().packageName, null)
                    )
                    intentSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intentSettings)
                }


                dexter.check()
            } else {
                showLongToast(
                    requireContext(),
                    "Ya se subió una imagen. Te pedimos que completes la creación del seguimiento"
                )
            }

        }

    }

    fun changeMode() {
        if(!userTracings.isEmpty()) {
            showInput = !showInput
            if(!showInput) {
                newConsultationNameTil.visibility = View.GONE
                spinner.visibility = View.VISIBLE
            } else {
                spinner.visibility = View.GONE
                newConsultationNameTil.visibility = View.VISIBLE
            }
        }
    }

    fun loadData() {
        val tracingRef = dataBase.collection("USER").document(user.uid).collection("TRACING")
        userTracings = ArrayList()
        tracingRef.get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    userTracings.add(document.id)
                }
                setAdapter()
            }
            .addOnFailureListener { exception ->
                if(!showInput) {
                    showLongToast(requireContext().applicationContext, "No fue posible cargar los seguimientos existentes")
                }
            }
    }

    fun setAdapter() {
        val adapter = ArrayAdapter(this.context, android.R.layout.simple_spinner_dropdown_item, userTracings)
        spinner.adapter = adapter
    }

    // Generates the image file
    @Throws(IOException::class)
    fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName: String = "NewConsultation_" + timeStamp + "_"
        val storageDir: File = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (!storageDir.exists()) storageDir.mkdirs()
        val imageFile = File.createTempFile(imageFileName, ".jpg", storageDir)
        if (imageFilePath != "") {
            var file = File(imageFilePath)
            file.delete()
        }
        imageFilePath = imageFile.absolutePath
        return imageFile
    }

    private fun disableHideInputs(enable: Boolean, button: Int, progressB: Int) {
        newConsultationName.isEnabled = enable
        spinner.isEnabled = enable
        changeModeButton.isEnabled = enable
        newConsultationImage.isEnabled = enable
        confirmButton.visibility = button
        progressBar.visibility = progressB
        galleryButton.isEnabled = enable
        cameraButton.isEnabled = enable
    }

    @OnClick(R.id.newConsultationMbGaleria)
    public fun openGallery(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(activity!!.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permissions, PERMISSION_CODE)
            }else{
                pickImageFromGallery()
            }
        }else {
            pickImageFromGallery()
        }
    }

    private fun pickImageFromGallery(){
        var intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if(grantResults.size>0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED){
                    pickImageFromGallery()
                }else{
                    showLongToast(requireContext(), "Debe aceptar el permiso para abrir la galeria")
                }

            }
        }
    }
}
