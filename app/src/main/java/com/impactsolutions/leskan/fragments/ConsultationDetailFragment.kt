package com.impactsolutions.leskan.fragments

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.impactsolutions.leskan.R
import com.impactsolutions.leskan.adapters.ConsultationsAdapter
import com.impactsolutions.leskan.global_functions.userId
import com.impactsolutions.leskan.global_functions.verifyAvailableNetwork
import com.impactsolutions.leskan.models.Consultation
import com.impactsolutions.leskan.models.Tracing
import java.util.ArrayList

class ConsultationDetailFragment : Fragment() {

    private lateinit var ivImageConsultation: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        verifyAvailableNetwork(this.requireActivity() as AppCompatActivity, false, local = true)
        var view = inflater.inflate(R.layout.fragment_consultation_detail, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivImageConsultation = view.findViewById(R.id.consultationDetailIv)

        Glide.with(context!!).asBitmap()
            .load(arguments?.getString("image"))
            .into(ivImageConsultation)

    }

    companion object {
        @JvmStatic
        fun newInstance(image: String) =
            ConsultationDetailFragment().apply {
                arguments = Bundle().apply {
                    putString("image", image)
                }
            }
    }
}
