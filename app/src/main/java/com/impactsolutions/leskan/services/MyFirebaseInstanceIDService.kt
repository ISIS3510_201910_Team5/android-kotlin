package com.impactsolutions.leskan.services

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

class MyFirebaseInstanceIDService: FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        var refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d("TOKEN", "Refreshed token: " + refreshedToken)
    }
}