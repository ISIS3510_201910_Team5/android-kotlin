package com.impactsolutions.leskan

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.impactsolutions.leskan.global_functions.showLongToast
import com.impactsolutions.leskan.global_functions.showNetworkErrorToast
import com.impactsolutions.leskan.global_functions.verifyAvailableNetwork

class ResetPasswordActivity : AppCompatActivity() {
    private var TAG = "ForgotPasswordActivity"
    //UI elements
    private lateinit var etEmail: EditText
    private lateinit var btnSubmit: Button
    private lateinit var progressBar: ProgressBar

    //Firebase references
    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resetpassword)
        initialise()
    }
    private fun initialise() {
        etEmail = findViewById(R.id.resetPasswordTiedEmail) as EditText
        btnSubmit = findViewById(R.id.resetPasswordButton) as Button
        progressBar = findViewById(R.id.resetPasswordProgressBar)
        mAuth = FirebaseAuth.getInstance()
    }

    fun sendPasswordResetEmail(view : View) {
        if (verifyAvailableNetwork(this, true)) {
            sendPasswordResetEmailFunction()
        }
    }

    fun sendPasswordResetEmailFunction() {
        val email = etEmail.text.toString()
        if (!TextUtils.isEmpty(email)) {
            enableLoading(true)
            mAuth!!
                .sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val message = "Correo enviado."
                        Log.d(TAG, message)
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                        updateUI()
                    } else {
                        try {
                            throw task.exception!!
                        } catch (exception: FirebaseAuthException) {
                            val code = exception.errorCode
                            Log.w(code, exception.message)

                            // Verifies the error code
                            when (code) {
                                "ERROR_INVALID_EMAIL" -> {
                                    etEmail.error = "La dirección de correo que ingresaste no es válida"
                                }
                                "ERROR_USER_NOT_FOUND" -> {
                                    etEmail.error = "No existe un usuario con el correo ingresado"
                                }
                                else -> {
                                    showLongToast(this, "Hubo un error en restableciendo la contraseña. Intenta nuevamente")
                                }
                            }
                        } catch (exception: FirebaseNetworkException) {
                            showNetworkErrorToast(this)
                        } catch (exception: Exception) {
                            showLongToast(this, "Hubo un error enviando el correo. Intenta nuevamente")
                        }
                    }
                }
            enableLoading(false)
        } else {
            Toast.makeText(this, "Ingresa el correo", Toast.LENGTH_SHORT).show()
        }
    }
    private fun updateUI() {
        val intent = Intent(this@ResetPasswordActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    // Finish current activity
    fun finishActivityReset(view: View) {
        finish()
    }

    private fun enableLoading(enable: Boolean) {
        etEmail.isEnabled = !enable
        if(enable){
            btnSubmit.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        }else{
            btnSubmit.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }

    }
}