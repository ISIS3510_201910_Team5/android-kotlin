package com.impactsolutions.leskan

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class LeskanGlideModule: AppGlideModule()