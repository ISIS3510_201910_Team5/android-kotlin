package com.impactsolutions.leskan.models

class Tracing(
    val name: String = "",
    val state: String = "",
    val colorChange: String = "",
    val sizeChange: String = "",
    val textureChange: String = "",
    val date: String = ""
    ){
}