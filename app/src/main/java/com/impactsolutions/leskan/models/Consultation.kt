package com.impactsolutions.leskan.models

class Consultation(
    val image: String = "",
    val date: String = "",
    val state: String = "",
    val country: String = "",
    val city: String = "",
    val id: String = ""
    ){
}