package com.impactsolutions.leskan.models

class User(
    var email: String = "",
    var firstName: String = "",
    var lastName: String = "",
    var sex: Int = 0,
    var birthDate: String = "",
    var skinTone: Double = 0.0,
    var skinToneImage: String = "",
    var commonIssue: Boolean = true,
    var familySkinCancer: Boolean = true,
    var id: String = ""){
    //...
}