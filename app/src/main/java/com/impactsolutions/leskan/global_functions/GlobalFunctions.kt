package com.impactsolutions.leskan.global_functions

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Region
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.impactsolutions.leskan.models.User


var key = "AKIAJSQJXUBNOVWC7AIA"
var secret = "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"
var bucketName = "leskan-app"
var currentUser = User("Cargando...", "CARG", "ANDO", 0, "Cargando...", 0.0, "", true, true)
var userId = ""
lateinit var toast: Toast

fun showLongToast(context: Context, message: String) {
    try {
        toast.cancel()
    } catch (e: Exception) {
        Log.w("No", e.message)
    }

    toast = Toast.makeText(context, message, Toast.LENGTH_LONG)

    toast.show()
}

fun showNetworkErrorToast(context: Context) {

    try {
        toast.cancel()
    } catch (e: Exception) {
        Log.w("No", e.message)
    }

    toast = Toast.makeText(
        context,
        "La operación no se pudo completar. Por favor revisa tu conexión de internet",
        Toast.LENGTH_LONG
    )

    toast.show()
}

fun encodeUserEmail(userEmail: String): String {
    return userEmail.replace(".", ",")
}

fun decodeUserEmail(userEmail: String): String {
    return userEmail.replace(",", ".")
}

fun isLoggedIn(): Boolean {
    return FirebaseAuth.getInstance().currentUser != null
}

fun updateCurrentUser() {
    var userAuth = FirebaseAuth.getInstance().currentUser
    userId = userAuth!!.uid
    val docRef = FirebaseFirestore.getInstance().collection("USER").document(userAuth!!.uid)
    docRef.get().addOnSuccessListener { documentSnapshot ->
        if (documentSnapshot.exists()) {

            val user = documentSnapshot.toObject(User::class.java)
            currentUser = User(
                user!!.email,
                user!!.firstName,
                user!!.lastName,
                user!!.sex,
                user!!.birthDate,
                user!!.skinTone,
                user!!.skinToneImage,
                user!!.commonIssue,
                user!!.familySkinCancer
            )
        } else {
            Log.w("ERROR", "Error al cargar la información del usuario actual")
        }
    }
}

fun getCredentials(): BasicAWSCredentials {
    return BasicAWSCredentials(key, secret)
}

fun getRegion(): Region {
    return Region.getRegion("")
}

fun getAWSBucketName(): String {
    return bucketName
}

fun verifyNetwork(activity: AppCompatActivity): Boolean {
    val connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = connectivityManager.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnected
}

fun verifyAvailableNetwork(activity: AppCompatActivity, operation: Boolean, local: Boolean = false): Boolean {
    val network = verifyNetwork(activity)

    try {
        toast.cancel()
    } catch (e: Exception) {
        Log.w("No", e.message)
    }

    if (!network) {
        if (local) {

            toast = Toast.makeText(
                activity,
                "No hay conexión a internet, por lo que mostraremos el resultado de tu última consulta",
                Toast.LENGTH_LONG
            )

            toast.show()
        } else {
            if (operation) {

                toast = Toast.makeText(activity, "Esta operación requiere conexión a internet", Toast.LENGTH_SHORT)

                toast.show()
            } else {

                toast = Toast.makeText(activity, "Por favor revisa tu conexión a internet", Toast.LENGTH_LONG)

                toast.show()
            }
        }
    }

    return network
}
