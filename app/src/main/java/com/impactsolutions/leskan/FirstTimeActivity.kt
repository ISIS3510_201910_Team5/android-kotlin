package com.impactsolutions.leskan

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.Region
import com.facebook.common.util.UriUtil
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.impactsolutions.leskan.global_functions.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class FirstTimeActivity : AppCompatActivity() {

    val CAMERA_REQUEST_CODE = 0
    lateinit var imageFilePath: String
    @BindView(R.id.firstTimeIvSkinToneImage)
    lateinit var firstTimeIvSkinToneImage: SimpleDraweeView
    private lateinit var txtCommonIssue: RadioGroup
    private lateinit var txtFamilySkinCancer: RadioGroup
    private lateinit var progressBar: ProgressBar
    private lateinit var dataBase: FirebaseFirestore
    private lateinit var saveInformationButton: Button
    private lateinit var user: FirebaseUser
    private var processing: Boolean = false;
    private lateinit var s3: AmazonS3Client
    private lateinit var transferUtility: TransferUtility
    private lateinit var transferObserver: TransferObserver
    private lateinit var skinToneImage: String
    private lateinit var functions: FirebaseFunctions
    private var s3ImageUrl = ""
    private var imageUploaded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (isLoggedIn()) {
            verifyAvailableNetwork(this, false)
            setContentView(R.layout.activity_first_time)
            ButterKnife.bind(this)

            // Make the reference to the view variables
            txtCommonIssue = findViewById(R.id.firstTimeRgCommonIssue)
            txtFamilySkinCancer = findViewById(R.id.firstTimeRgFamilySkinCancer)
            progressBar = findViewById(R.id.firstTimeProgressBar)
            saveInformationButton = findViewById(R.id.firstTimeButton)
            dataBase = FirebaseFirestore.getInstance()
            imageFilePath = ""
            functions = FirebaseFunctions.getInstance()
            var currentUser = FirebaseAuth.getInstance().currentUser
            if (currentUser != null) {
                user = currentUser
            } else {
                finish()
                startActivity(Intent(this, MainActivity::class.java))
            }
        } else {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
/*                if(resultCode == Activity.RESULT_OK && data != null) {
                    photoImageView.setImageBitmap(data.extras.get("data") as Bitmap)
                }*/
                if (resultCode == Activity.RESULT_OK) {
                    val imgUri = Uri.Builder()
                        .scheme(UriUtil.LOCAL_FILE_SCHEME)
                        .path(imageFilePath)
                        .build()
                    firstTimeIvSkinToneImage.setImageURI(imgUri, this)
                }
            }
            else -> {
                showLongToast(this@FirstTimeActivity, "Error reconociendo el código de la imagen")
            }
        }
    }

    // Function called by the button
    fun saveFirstTimeInformation(view: View) {
        if (verifyAvailableNetwork(this, false)) {
            saveInformation()
        }
    }

    // Save the information on the database
    private fun saveInformation() {

        val commonIssueSelection = txtCommonIssue.checkedRadioButtonId
        val familySkinCancerSelection = txtFamilySkinCancer.checkedRadioButtonId

        if (commonIssueSelection != -1 && familySkinCancerSelection != -1 && imageFilePath.isNotEmpty()) {

            val commonIssue: String = (findViewById<RadioButton>(commonIssueSelection)).text.toString()

            val familySkinCancer: String =
                (findViewById<RadioButton>(familySkinCancerSelection)).text.toString()
            saveInformationButton.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
            disableInputs(false)

            if (imageUploaded) {
                saveInformationOnUser(getValue(commonIssue), getValue(familySkinCancer))
            } else {
                var file = File(imageFilePath)

                skinToneImage = user.uid + "/skinToneImage/" + user.uid + ".jpg"

                s3 = AmazonS3Client(getCredentials(), Region.SA_SaoPaulo.toAWSRegion())

                transferUtility = TransferUtility.builder().s3Client(s3).context(this).build()

                transferObserver = transferUtility.upload(
                    getAWSBucketName(),  //this is the bucket name on S3
                    skinToneImage,
                    file,
                    CannedAccessControlList.PublicRead
                )

                transferObserver.setTransferListener(object : TransferListener {
                    override fun onStateChanged(id: Int, state: TransferState?) {
                        if (TransferState.COMPLETED == state) {
                            Log.i("UPLOAD_STATE", "COMPLETED")
                            file.delete()
                            imageUploaded = true
                            saveInformationOnUser(getValue(commonIssue), getValue(familySkinCancer))
                        }
                    }

                    override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                        val percentage = bytesCurrent.toFloat() / bytesTotal.toFloat() * 100
                        Log.d("percentage", "" + percentage)
                    }

                    override fun onError(id: Int, ex: Exception?) {
                        showLongToast(this@FirstTimeActivity, "Hubo un error subiendo la imagen. Intenta de nuevo subir la misma imagen")
                        disableInputs(true)
                        progressBar.visibility = View.GONE
                        saveInformationButton.visibility = View.VISIBLE
                        processing = false
                    }
                })
            }
        } else {
            processing = false
            showLongToast(this, "Por favor completa todos los campos")
        }

    }

    private fun saveInformationOnUser(commonIssue: Boolean, familySkinCancer: Boolean) {

        val sfDocRef = dataBase.collection("USER").document(user.uid)
        s3ImageUrl = s3.getUrl(getAWSBucketName(), skinToneImage).toExternalForm().toString()
        dataBase.runTransaction { transaction ->
            val snapshot = transaction.get(sfDocRef)
            transaction.update(sfDocRef, "commonIssue", commonIssue)
            transaction.update(sfDocRef, "familySkinCancer", familySkinCancer)

            transaction.update(sfDocRef, "skinToneImage", s3ImageUrl)
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                defineSkinTone(s3ImageUrl)
            } else {
                try {
                    throw task.exception!!
                } catch (exception: FirebaseNetworkException) {
                    showNetworkErrorToast(this)
                } catch (exception: Exception) {
                    showLongToast(this, "Hubo un error guardando la información")
                }
                disableInputs(true)
                progressBar.visibility = View.GONE
                saveInformationButton.visibility = View.VISIBLE
                processing = false
            }
        }
    }

    // Disable the inputs based on a parameter
    private fun disableInputs(enable: Boolean) {
        txtCommonIssue.getChildAt(0).isEnabled = enable
        txtCommonIssue.getChildAt(1).isEnabled = enable
        txtFamilySkinCancer.getChildAt(0).isEnabled = enable
        txtFamilySkinCancer.getChildAt(1).isEnabled = enable
    }

    private fun getValue(selection: String): Boolean {
        return selection == getString(R.string.si)
    }

    @OnClick(R.id.firstTimeIvSkinToneImage)
    fun onClickSkinToneImage() {
        if (!processing) {
            Dexter.withActivity(this)
                .withPermissions(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<PermissionRequest>?,
                        token: PermissionToken?
                    ) {
                        try {
                            var alertDialog = AlertDialog.Builder(this@FirstTimeActivity.applicationContext)
                                .setTitle("getString(R.string.storage_permission_rationale_title)")
                                .setMessage("getString(R.string.storage_permission_rationale_message)")
                                .setNegativeButton(
                                    android.R.string.cancel,
                                    DialogInterface.OnClickListener { dialogInterface, i ->
                                        dialogInterface.dismiss()
                                        token?.cancelPermissionRequest()
                                    })
                                .setPositiveButton(
                                    android.R.string.ok,
                                    DialogInterface.OnClickListener { dialogInterface, i ->
                                        dialogInterface.dismiss()
                                        token?.continuePermissionRequest()
                                    })
                            alertDialog.show()
                        } catch (e: Exception) {
                            showLongToast(this@FirstTimeActivity, "Activa los permisos y reinicia la aplicación para continuar")
                            var intentSettings = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", this@FirstTimeActivity.packageName, null)
                            )
                            intentSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intentSettings)
                        } catch (er: Error) {
                            showLongToast(this@FirstTimeActivity, "Activa los permisos y reinicia la aplicación para continuar")
                            var intentSettings = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", this@FirstTimeActivity.packageName, null)
                            )
                            intentSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intentSettings)
                        }

                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report?.areAllPermissionsGranted()!!) {

                            try {
                                val imageFile = createImageFile()
                                val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                if (callCameraIntent.resolveActivity(packageManager) != null) {
                                    val authorities = packageName + ".fileprovider"
                                    val imageUri =
                                        FileProvider.getUriForFile(this@FirstTimeActivity, authorities, imageFile)
                                    callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                                    startActivityForResult(callCameraIntent, CAMERA_REQUEST_CODE)
                                }
                            } catch (e: IOException) {
                                showLongToast(this@FirstTimeActivity, "No se pudo crear la imagen")
                            }
                        }

                    }

                }
                ).check()
        }
    }

    @Throws(IOException::class)
    fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName: String = "SkinTone_" + timeStamp + "_"
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (!storageDir.exists()) storageDir.mkdirs()
        val imageFile = File.createTempFile(imageFileName, ".jpg", storageDir)
        imageFilePath = imageFile.absolutePath
        return imageFile
    }




    // Cloud function implementation
    private fun defineSkinToneFunction(s3ImageUrl: String): Task<HashMap<String, Object>> {
        // Create the arguments to the callable function.
        val data = hashMapOf(
            "user" to user.uid,
            "url" to s3ImageUrl
        )

        return functions
            .getHttpsCallable("rapidResponseASC")
            .call(data)
            .continueWith { task ->
                val result = task.result?.data as HashMap<String, Object>
                result
            }
    }

    // Cloud function call
    private fun defineSkinTone(s3ImageUrl: String) {
        defineSkinToneFunction(s3ImageUrl)
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details
                    }

                    showLongToast(this@FirstTimeActivity, "Hubo un error guardando la información")
                    disableInputs(true)
                    progressBar.visibility = View.GONE
                    saveInformationButton.visibility = View.VISIBLE
                    processing = false

                } else {
                    showLongToast(this, "Tu información se guardó correctamente")
                    finishAffinity()
                    startActivity(Intent(this, HomeActivity::class.java))
                }
            })
    }


}
