package com.impactsolutions.leskan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import com.impactsolutions.leskan.global_functions.*
import com.impactsolutions.leskan.models.User

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        verifyAvailableNetwork(this, false)
        super.onCreate(savedInstanceState)
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        Fresco.initialize(this)
        if (isLoggedIn()) {
            updateCurrentUser()
            setContentView(R.layout.activity_main_loading)
            checkFirstTime()
        } else {
            setContentView(R.layout.activity_main)
        }
    }

    private fun checkFirstTime() {
        val docRef =
            FirebaseFirestore.getInstance().collection("USER").document(FirebaseAuth.getInstance().currentUser!!.uid)
        docRef.get().addOnSuccessListener { documentSnapshot ->
            if (documentSnapshot.exists()) {
                val user = documentSnapshot.toObject(User::class.java)
                if (user!!.skinToneImage != "") {
                    finishAffinity()
                    startActivity(Intent(this, HomeActivity::class.java))
                } else {
                    finishAffinity()
                    startActivity(Intent(this, FirstTimeActivity::class.java))
                }
            } else {
                finishAffinity()
                startActivity(Intent(this, HomeActivity::class.java))
            }
        }
    }

    fun goToLogin(view: View) {
        if (verifyAvailableNetwork(this, true)) {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    fun goToRegister(view: View) {
        if (verifyAvailableNetwork(this, true)) {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }

}
